// Agregar a Local Storage
localStorage.setItem('nombre', 'Pablo')

// Sesion Storage
//sessionStorage.setItem('nombre', 'Pablo')

// Eliminar de Local Storage
//localStorage.removeItem('nombre')

const nombre = localStorage.getItem('nombre')

console.log(nombre)

// Eliminar de Local Storage
localStorage.clear()
